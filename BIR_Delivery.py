#!/usr/bin/python3.6

###########################################################################
#
# Script  : BIR_Delivery.py
# Author  : Kevin Novini
# Version : 1.0 July 2020
# Purpose : Reads a config file listing BIR reports and extracts
#           and determines whether to send them over SFTP, email or both
#           based on the parameters set. Once the transmission has been
#           completed the file is archived.
#
###########################################################################

# import any libs

import boto3
import io
import os
import sys
import configparser
from zipfile import ZipFile

# set variables
#archive_folder = '/home/kevin.novini/archive/'

s3 = boto3.client('s3')
my_bucket = 'blackwattle-test-s3'
ini_file = 'config/delivery.cfg'

# read INI file

try:
    obj = s3.get_object(Bucket=my_bucket, Key=ini_file)
    config = configparser.ConfigParser()
    config.read_string(obj['Body'].read().decode())
except IOError:
    print('INI file ' + ini_file + ' is not accessible. Aborting!')
    sys.exit()

# look up filename in INI file
# and determine transmission method

def lookup_file(fname):
    try:
        if config.has_section(fname):
            method = config.get(fname, 'method')
            methods = method.split(',')
            for meth in methods:
                meth = meth.replace('"', '')
                print('method is : ' + ':' + meth + ':')
                if meth == 'SFTP':
                    send_SFTP(fname)
                elif meth == 'email':
                    send_email(fname)
                else:
                    print('Doing nothing')
        else:
            print('FAIL: ' + fname + ' is not found in INI file')
    except Exception as e:
        print(e)


def send_SFTP(fname):
    print('doing SFTP')

    # test if settings are defined

    for settings in ['dest_server', 'dest_port', 'dest_folder', 'user']:
        if not config.has_option(fname, settings):
            print('FAIL: %s.%s : %s' % (fname, settings, 'Config setting not found in INI file!'))
            return

    print('dest_server : ' + config.get(fname, 'dest_server'))
    print(config.get(fname, 'dest_port'))
    print(config.get(fname, 'dest_folder'))
    print(config.get(fname, 'user'))


def send_email(fname):
    print('doing email')

    # test if settings are defined

    for settings in ['to', 'from', 'subject']:
        if not config.has_option(fname, settings):
            print('FAIL: %s.%s : %s' % (fname, settings, 'Config setting not found in INI file!'))
            return

    print(config.get(fname, 'to'))
    print(config.get(fname, 'from'))
    print(config.get(fname, 'subject'))


# process files in turn

def process_file(filename):

    # remove file date

    fname = filename[0:-13]
    print('File is ' + fname)
    lookup_file(fname)
    print()


# archive the file

def archive_file(filename):

    arcfile = 'archive/' + filename
    srcfile = 'incoming/' + filename
    print('Copying ' + srcfile + ' to ' + arcfile)
    # Copy object A as object B
    try:
        #client.copy(CopySource=srcfile, Bucket=my_bucket, Key=arcfile)
        s3.copy_object(CopySource=srcfile, Bucket=my_bucket, Key=arcfile)
        #s3.Object(my_bucket, arcfile).copy_from(CopySource=srcfile)
    except:
        print('Unable to copy file' + filename + ' to archive location ' + arcfile)

    # remove original file

    try:
        print('Removing ' + filetozip)
    except:
       #os.remove(filetozip)
       print('Error while deleting file ' + filetozip)


def main(event, context):

    # read incoming directory

    try:
        for file in s3.list_objects(Bucket=my_bucket, Prefix='incoming/')['Contents']:
                file = file['Key'].replace('incoming/' , '')
                if file:
                    print(file)
                    process_file(file)
                    #archive_file(file)
    except:
        print('FAIL: Unable to read incoming folder')


# script starts here

if __name__ == '__main__':
    main()

